package com.gitlab.psykongoroo.javamud.grid;

import com.gitlab.psykongoroo.javamud.grid.event.GridUpdateEvent;
import com.gitlab.psykongoroo.javamud.grid.model.GridViewEntity;
import com.gitlab.psykongoroo.javamud.grid.repository.GridItemRepository;
import com.gitlab.psykongoroo.javamud.grid.repository.GridViewRepository;
import com.gitlab.psykongoroo.javamud.grid.resource.GridItem;
import com.gitlab.psykongoroo.javamud.grid.resource.GridView;
import com.gitlab.psykongoroo.javamud.utils.ConverterUtils;
import org.dozer.Mapper;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static com.gitlab.psykongoroo.javamud.utils.StreamUtils.streamOf;
import static java.lang.String.format;

@RestController
public class GridController implements ConverterUtils {

  private final Mapper dozer;
  private final GridItemRepository gridItemRepository;
  private final GridViewRepository gridViewRepository;
  private final SimpMessagingTemplate template;

  @java.beans.ConstructorProperties({"dozer", "gridItemRepository", "gridViewRepository", "template"})
  public GridController(Mapper dozer, GridItemRepository gridItemRepository, GridViewRepository gridViewRepository, SimpMessagingTemplate template) {
    this.dozer = dozer;
    this.gridItemRepository = gridItemRepository;
    this.gridViewRepository = gridViewRepository;
    this.template = template;
  }

  @Transactional
  @MessageMapping("/grid/update/event")
  public void update(GridUpdateEvent event) {
    gridItemRepository.saveAll(event
        .getGridItems()
        .stream()
        .map(this::toGridItemEntity)
        .peek(e -> e.setGridId(event.getGridId()))
        ::iterator);
    sendGridItemToGridViews(event.getGridId());
  }

  public void sendGridItemToGridViews(Long gridId) {
    final String topicFormat = "/topic/grid/%d/view/%d";
    streamOf(gridViewRepository
        .findByGridId(gridId))
        .map(this::extendGridView)
        .forEach(view -> template.convertAndSend(
            format(topicFormat, view.getGridId(), view.getId()),
            view));
  }

  @RequestMapping(value = "/api/grid/view", method = RequestMethod.POST)
  public GridView postGridView(
      @RequestBody
      @Validated({GridView.Post.class}) GridView g) {
    GridViewEntity e = dozer.map(g, GridViewEntity.class);
    e = gridViewRepository.save(e);
    return extendGridView(e);
  }

  private GridView extendGridView(GridViewEntity e) {
    GridView view = dozer.map(e, GridView.class);
    List<GridItem> items = streamOf(
        gridItemRepository
            .findByGridIdEnclosed(
                e.getGridId(),
                e.getMinX(), e.getMaxX(),
                e.getMinY(), e.getMaxY()))
        .map(this::toGridItem)
        .collect(Collectors.toList());
    view.setGridItems(items);
    return view;
  }

  public Mapper getDozer() {
    return this.dozer;
  }
}
