package com.gitlab.psykongoroo.javamud.grid.event;

import com.gitlab.psykongoroo.javamud.grid.resource.GridItem;

import java.util.List;

public class GridUpdateEvent {

  private long gridId;
  private List<GridItem> gridItems;

  @java.beans.ConstructorProperties({"gridId", "gridItems"})
  public GridUpdateEvent(long gridId, List<GridItem> gridItems) {
    this.gridId = gridId;
    this.gridItems = gridItems;
  }

  public GridUpdateEvent() {
  }

  public long getGridId() {
    return this.gridId;
  }

  public List<GridItem> getGridItems() {
    return this.gridItems;
  }

  public void setGridId(long gridId) {
    this.gridId = gridId;
  }

  public void setGridItems(List<GridItem> gridItems) {
    this.gridItems = gridItems;
  }

  public boolean equals(Object o) {
    if (o == this) return true;
    if (!(o instanceof GridUpdateEvent)) return false;
    final GridUpdateEvent other = (GridUpdateEvent) o;
    if (!other.canEqual((Object) this)) return false;
    if (this.getGridId() != other.getGridId()) return false;
    final Object this$gridItems = this.getGridItems();
    final Object other$gridItems = other.getGridItems();
    if (this$gridItems == null ? other$gridItems != null : !this$gridItems.equals(other$gridItems)) return false;
    return true;
  }

  public int hashCode() {
    final int PRIME = 59;
    int result = 1;
    final long $gridId = this.getGridId();
    result = result * PRIME + (int) ($gridId >>> 32 ^ $gridId);
    final Object $gridItems = this.getGridItems();
    result = result * PRIME + ($gridItems == null ? 43 : $gridItems.hashCode());
    return result;
  }

  protected boolean canEqual(Object other) {
    return other instanceof GridUpdateEvent;
  }

  public String toString() {
    return "GridUpdateEvent(gridId=" + this.getGridId() + ", gridItems=" + this.getGridItems() + ")";
  }
}
