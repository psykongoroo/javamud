package com.gitlab.psykongoroo.javamud.grid.model;

import javax.persistence.*;

@Entity
public class GridEntity {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  private long width;

  @Column
  private long height;

  @java.beans.ConstructorProperties({"id", "width", "height"})
  public GridEntity(Long id, long width, long height) {
    this.id = id;
    this.width = width;
    this.height = height;
  }

  public GridEntity() {
  }

  public Long getId() {
    return this.id;
  }

  public long getWidth() {
    return this.width;
  }

  public long getHeight() {
    return this.height;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setWidth(long width) {
    this.width = width;
  }

  public void setHeight(long height) {
    this.height = height;
  }

  public boolean equals(Object o) {
    if (o == this) return true;
    if (!(o instanceof GridEntity)) return false;
    final GridEntity other = (GridEntity) o;
    if (!other.canEqual((Object) this)) return false;
    final Object this$id = this.getId();
    final Object other$id = other.getId();
    if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
    if (this.getWidth() != other.getWidth()) return false;
    if (this.getHeight() != other.getHeight()) return false;
    return true;
  }

  public int hashCode() {
    final int PRIME = 59;
    int result = 1;
    final Object $id = this.getId();
    result = result * PRIME + ($id == null ? 43 : $id.hashCode());
    final long $width = this.getWidth();
    result = result * PRIME + (int) ($width >>> 32 ^ $width);
    final long $height = this.getHeight();
    result = result * PRIME + (int) ($height >>> 32 ^ $height);
    return result;
  }

  protected boolean canEqual(Object other) {
    return other instanceof GridEntity;
  }

  public String toString() {
    return "GridEntity(id=" + this.getId() + ", width=" + this.getWidth() + ", height=" + this.getHeight() + ")";
  }
}
