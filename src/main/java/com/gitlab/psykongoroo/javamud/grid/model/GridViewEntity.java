package com.gitlab.psykongoroo.javamud.grid.model;

import javax.persistence.*;

@Entity
public class GridViewEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column
  private Long id;

  @Column
  private long gridId;
  @Column
  private long minX;
  @Column
  private long maxX;
  @Column
  private long minY;
  @Column
  private long maxY;

  @java.beans.ConstructorProperties({"id", "gridId", "minX", "maxX", "minY", "maxY"})
  public GridViewEntity(Long id, long gridId, long minX, long maxX, long minY, long maxY) {
    this.id = id;
    this.gridId = gridId;
    this.minX = minX;
    this.maxX = maxX;
    this.minY = minY;
    this.maxY = maxY;
  }

  public GridViewEntity() {
  }

  public Long getId() {
    return this.id;
  }

  public long getGridId() {
    return this.gridId;
  }

  public long getMinX() {
    return this.minX;
  }

  public long getMaxX() {
    return this.maxX;
  }

  public long getMinY() {
    return this.minY;
  }

  public long getMaxY() {
    return this.maxY;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setGridId(long gridId) {
    this.gridId = gridId;
  }

  public void setMinX(long minX) {
    this.minX = minX;
  }

  public void setMaxX(long maxX) {
    this.maxX = maxX;
  }

  public void setMinY(long minY) {
    this.minY = minY;
  }

  public void setMaxY(long maxY) {
    this.maxY = maxY;
  }

  public boolean equals(Object o) {
    if (o == this) return true;
    if (!(o instanceof GridViewEntity)) return false;
    final GridViewEntity other = (GridViewEntity) o;
    if (!other.canEqual((Object) this)) return false;
    final Object this$id = this.getId();
    final Object other$id = other.getId();
    if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
    if (this.getGridId() != other.getGridId()) return false;
    if (this.getMinX() != other.getMinX()) return false;
    if (this.getMaxX() != other.getMaxX()) return false;
    if (this.getMinY() != other.getMinY()) return false;
    if (this.getMaxY() != other.getMaxY()) return false;
    return true;
  }

  public int hashCode() {
    final int PRIME = 59;
    int result = 1;
    final Object $id = this.getId();
    result = result * PRIME + ($id == null ? 43 : $id.hashCode());
    final long $gridId = this.getGridId();
    result = result * PRIME + (int) ($gridId >>> 32 ^ $gridId);
    final long $minX = this.getMinX();
    result = result * PRIME + (int) ($minX >>> 32 ^ $minX);
    final long $maxX = this.getMaxX();
    result = result * PRIME + (int) ($maxX >>> 32 ^ $maxX);
    final long $minY = this.getMinY();
    result = result * PRIME + (int) ($minY >>> 32 ^ $minY);
    final long $maxY = this.getMaxY();
    result = result * PRIME + (int) ($maxY >>> 32 ^ $maxY);
    return result;
  }

  protected boolean canEqual(Object other) {
    return other instanceof GridViewEntity;
  }

  public String toString() {
    return "GridViewEntity(id=" + this.getId() + ", gridId=" + this.getGridId() + ", minX=" + this.getMinX() + ", maxX=" + this.getMaxX() + ", minY=" + this.getMinY() + ", maxY=" + this.getMaxY() + ")";
  }
}
