package com.gitlab.psykongoroo.javamud.grid.repository;

import com.gitlab.psykongoroo.javamud.grid.model.GridItemEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface GridItemRepository extends CrudRepository<GridItemEntity, Long> {

  @Query(value = "SELECT e.* " +
      "FROM main.grid_item_rtree r " +
      "JOIN main.grid_item_entity e ON r.id = e.id " +
      "WHERE r.minX>= :minX AND r.maxX <= :maxX " +
      "AND r.minY>= :minY AND r.maxY <= :maxY " +
      "AND e.grid_id = :gridId ", nativeQuery = true)
  Iterable<GridItemEntity> findByGridIdEnclosed(
      @Param("gridId") long gridId,
      @Param("minX") long minX,
      @Param("maxX") long maxX,
      @Param("minY") long minY,
      @Param("maxY") long maxY
  );

}
