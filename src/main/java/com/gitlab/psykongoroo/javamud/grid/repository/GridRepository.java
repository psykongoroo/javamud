package com.gitlab.psykongoroo.javamud.grid.repository;

import com.gitlab.psykongoroo.javamud.grid.model.GridEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GridRepository extends CrudRepository<GridEntity, Long> {
}
