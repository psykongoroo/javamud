package com.gitlab.psykongoroo.javamud.grid.repository;

import com.gitlab.psykongoroo.javamud.grid.model.GridViewEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface GridViewRepository extends CrudRepository<GridViewEntity, Long> {

  Iterable<GridViewEntity> findByGridId(long gridId);

}
