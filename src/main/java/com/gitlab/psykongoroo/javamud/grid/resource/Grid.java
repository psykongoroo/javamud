package com.gitlab.psykongoroo.javamud.grid.resource;

import java.util.List;

public class Grid {

  private long width;
  private long height;
  private List<GridItem> gridItems;

  @java.beans.ConstructorProperties({"width", "height", "gridItems"})
  public Grid(long width, long height, List<GridItem> gridItems) {
    this.width = width;
    this.height = height;
    this.gridItems = gridItems;
  }

  public Grid() {
  }

  public long getWidth() {
    return this.width;
  }

  public long getHeight() {
    return this.height;
  }

  public List<GridItem> getGridItems() {
    return this.gridItems;
  }

  public void setWidth(long width) {
    this.width = width;
  }

  public void setHeight(long height) {
    this.height = height;
  }

  public void setGridItems(List<GridItem> gridItems) {
    this.gridItems = gridItems;
  }

  public boolean equals(Object o) {
    if (o == this) return true;
    if (!(o instanceof Grid)) return false;
    final Grid other = (Grid) o;
    if (!other.canEqual((Object) this)) return false;
    if (this.getWidth() != other.getWidth()) return false;
    if (this.getHeight() != other.getHeight()) return false;
    final Object this$gridItems = this.getGridItems();
    final Object other$gridItems = other.getGridItems();
    if (this$gridItems == null ? other$gridItems != null : !this$gridItems.equals(other$gridItems)) return false;
    return true;
  }

  public int hashCode() {
    final int PRIME = 59;
    int result = 1;
    final long $width = this.getWidth();
    result = result * PRIME + (int) ($width >>> 32 ^ $width);
    final long $height = this.getHeight();
    result = result * PRIME + (int) ($height >>> 32 ^ $height);
    final Object $gridItems = this.getGridItems();
    result = result * PRIME + ($gridItems == null ? 43 : $gridItems.hashCode());
    return result;
  }

  protected boolean canEqual(Object other) {
    return other instanceof Grid;
  }

  public String toString() {
    return "Grid(width=" + this.getWidth() + ", height=" + this.getHeight() + ", gridItems=" + this.getGridItems() + ")";
  }
}
