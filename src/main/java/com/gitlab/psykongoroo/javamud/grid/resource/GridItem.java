package com.gitlab.psykongoroo.javamud.grid.resource;

public class GridItem {

  private Long id;
  private long gridId;
  private long x;
  private long y;

  @java.beans.ConstructorProperties({"id", "gridId", "x", "y"})
  public GridItem(Long id, long gridId, long x, long y) {
    this.id = id;
    this.gridId = gridId;
    this.x = x;
    this.y = y;
  }

  public GridItem() {
  }

  public Long getId() {
    return this.id;
  }

  public long getGridId() {
    return this.gridId;
  }

  public long getX() {
    return this.x;
  }

  public long getY() {
    return this.y;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setGridId(long gridId) {
    this.gridId = gridId;
  }

  public void setX(long x) {
    this.x = x;
  }

  public void setY(long y) {
    this.y = y;
  }

  public boolean equals(Object o) {
    if (o == this) return true;
    if (!(o instanceof GridItem)) return false;
    final GridItem other = (GridItem) o;
    if (!other.canEqual((Object) this)) return false;
    final Object this$id = this.getId();
    final Object other$id = other.getId();
    if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
    if (this.getGridId() != other.getGridId()) return false;
    if (this.getX() != other.getX()) return false;
    if (this.getY() != other.getY()) return false;
    return true;
  }

  public int hashCode() {
    final int PRIME = 59;
    int result = 1;
    final Object $id = this.getId();
    result = result * PRIME + ($id == null ? 43 : $id.hashCode());
    final long $gridId = this.getGridId();
    result = result * PRIME + (int) ($gridId >>> 32 ^ $gridId);
    final long $x = this.getX();
    result = result * PRIME + (int) ($x >>> 32 ^ $x);
    final long $y = this.getY();
    result = result * PRIME + (int) ($y >>> 32 ^ $y);
    return result;
  }

  protected boolean canEqual(Object other) {
    return other instanceof GridItem;
  }

  public String toString() {
    return "GridItem(id=" + this.getId() + ", gridId=" + this.getGridId() + ", x=" + this.getX() + ", y=" + this.getY() + ")";
  }
}
