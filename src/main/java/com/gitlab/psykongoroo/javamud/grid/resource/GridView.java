package com.gitlab.psykongoroo.javamud.grid.resource;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;

public class GridView {


  @Null(groups = Post.class)
  private Long id;
  @NotNull(groups = Post.class)
  private Long gridId;
  @NotNull(groups = Post.class)
  private Long minX;
  @NotNull(groups = Post.class)
  private Long maxX;
  @NotNull(groups = Post.class)
  private Long minY;
  @NotNull(groups = Post.class)
  private Long maxY;

  private List<GridItem> gridItems;

  @java.beans.ConstructorProperties({"id", "gridId", "minX", "maxX", "minY", "maxY", "gridItems"})
  public GridView(@Null(groups = Post.class) Long id, @NotNull(groups = Post.class) Long gridId, @NotNull(groups = Post.class) Long minX, @NotNull(groups = Post.class) Long maxX, @NotNull(groups = Post.class) Long minY, @NotNull(groups = Post.class) Long maxY, List<GridItem> gridItems) {
    this.id = id;
    this.gridId = gridId;
    this.minX = minX;
    this.maxX = maxX;
    this.minY = minY;
    this.maxY = maxY;
    this.gridItems = gridItems;
  }

  public GridView() {
  }

  public @Null(groups = Post.class) Long getId() {
    return this.id;
  }

  public @NotNull(groups = Post.class) Long getGridId() {
    return this.gridId;
  }

  public @NotNull(groups = Post.class) Long getMinX() {
    return this.minX;
  }

  public @NotNull(groups = Post.class) Long getMaxX() {
    return this.maxX;
  }

  public @NotNull(groups = Post.class) Long getMinY() {
    return this.minY;
  }

  public @NotNull(groups = Post.class) Long getMaxY() {
    return this.maxY;
  }

  public List<GridItem> getGridItems() {
    return this.gridItems;
  }

  public void setId(@Null(groups = Post.class) Long id) {
    this.id = id;
  }

  public void setGridId(@NotNull(groups = Post.class) Long gridId) {
    this.gridId = gridId;
  }

  public void setMinX(@NotNull(groups = Post.class) Long minX) {
    this.minX = minX;
  }

  public void setMaxX(@NotNull(groups = Post.class) Long maxX) {
    this.maxX = maxX;
  }

  public void setMinY(@NotNull(groups = Post.class) Long minY) {
    this.minY = minY;
  }

  public void setMaxY(@NotNull(groups = Post.class) Long maxY) {
    this.maxY = maxY;
  }

  public void setGridItems(List<GridItem> gridItems) {
    this.gridItems = gridItems;
  }

  public boolean equals(Object o) {
    if (o == this) return true;
    if (!(o instanceof GridView)) return false;
    final GridView other = (GridView) o;
    if (!other.canEqual((Object) this)) return false;
    final Object this$id = this.getId();
    final Object other$id = other.getId();
    if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
    final Object this$gridId = this.getGridId();
    final Object other$gridId = other.getGridId();
    if (this$gridId == null ? other$gridId != null : !this$gridId.equals(other$gridId)) return false;
    final Object this$minX = this.getMinX();
    final Object other$minX = other.getMinX();
    if (this$minX == null ? other$minX != null : !this$minX.equals(other$minX)) return false;
    final Object this$maxX = this.getMaxX();
    final Object other$maxX = other.getMaxX();
    if (this$maxX == null ? other$maxX != null : !this$maxX.equals(other$maxX)) return false;
    final Object this$minY = this.getMinY();
    final Object other$minY = other.getMinY();
    if (this$minY == null ? other$minY != null : !this$minY.equals(other$minY)) return false;
    final Object this$maxY = this.getMaxY();
    final Object other$maxY = other.getMaxY();
    if (this$maxY == null ? other$maxY != null : !this$maxY.equals(other$maxY)) return false;
    final Object this$gridItems = this.getGridItems();
    final Object other$gridItems = other.getGridItems();
    if (this$gridItems == null ? other$gridItems != null : !this$gridItems.equals(other$gridItems)) return false;
    return true;
  }

  public int hashCode() {
    final int PRIME = 59;
    int result = 1;
    final Object $id = this.getId();
    result = result * PRIME + ($id == null ? 43 : $id.hashCode());
    final Object $gridId = this.getGridId();
    result = result * PRIME + ($gridId == null ? 43 : $gridId.hashCode());
    final Object $minX = this.getMinX();
    result = result * PRIME + ($minX == null ? 43 : $minX.hashCode());
    final Object $maxX = this.getMaxX();
    result = result * PRIME + ($maxX == null ? 43 : $maxX.hashCode());
    final Object $minY = this.getMinY();
    result = result * PRIME + ($minY == null ? 43 : $minY.hashCode());
    final Object $maxY = this.getMaxY();
    result = result * PRIME + ($maxY == null ? 43 : $maxY.hashCode());
    final Object $gridItems = this.getGridItems();
    result = result * PRIME + ($gridItems == null ? 43 : $gridItems.hashCode());
    return result;
  }

  protected boolean canEqual(Object other) {
    return other instanceof GridView;
  }

  public String toString() {
    return "GridView(id=" + this.getId() + ", gridId=" + this.getGridId() + ", minX=" + this.getMinX() + ", maxX=" + this.getMaxX() + ", minY=" + this.getMinY() + ", maxY=" + this.getMaxY() + ", gridItems=" + this.getGridItems() + ")";
  }

  public interface Post{}
}
