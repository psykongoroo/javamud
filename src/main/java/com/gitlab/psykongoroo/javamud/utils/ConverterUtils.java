package com.gitlab.psykongoroo.javamud.utils;

import com.gitlab.psykongoroo.javamud.grid.model.GridItemEntity;
import com.gitlab.psykongoroo.javamud.grid.resource.GridItem;
import org.dozer.Mapper;

public interface ConverterUtils {

  Mapper getDozer();

  default GridItem toGridItem(GridItemEntity e) {
    return getDozer().map(e, GridItem.class);
  }

  default GridItemEntity toGridItemEntity(GridItem r) {
    return getDozer().map(r, GridItemEntity.class);
  }
}
