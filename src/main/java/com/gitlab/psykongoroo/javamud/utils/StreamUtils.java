package com.gitlab.psykongoroo.javamud.utils;

import java.util.stream.Stream;

public class StreamUtils {

  public static <T> Stream<T> streamOf(Iterable<T> all) {
    return java.util.stream.StreamSupport.stream(all.spliterator(), false);
  }

}
