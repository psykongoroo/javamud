package com.gitlab.psykongoroo.javamud.utils;

import java.util.Vector;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

public class Vector2 {

    private int X;
    private int Y;


    public Vector2(int X, int Y) {
        this.X = X;
        this.Y = Y;
    }

    public Vector2 Vector2Addition(Vector2 first, Vector2 second) {
        return new Vector2( first.getX() + second.getX(), second.getY() + first.getY());
    }

    public Vector2 Vector2ScalarMultiplication(int value, Vector2 vector) {
        return new Vector2(vector.getX() * value, vector.getY() * value);
    }

    public int Vector2DotProduct(Vector2 vector, Vector2 vector2) {
        return vector.getX() * vector2.getX() + vector.getY() * vector2.getY();
    }

    public double Vector2AngleBetweenVectors(Vector2 vector, Vector2 vector2) {
        double magnitudePowerVector1 = (vector.getX() * vector.getX()) + (vector.getY() * vector.getY());
        double magnitudePowerVector2 = (vector2.getX() * vector2.getX()) + (vector2.getY() * vector2.getY());
        double magnitudeVector1 = sqrt(magnitudePowerVector1);
        double magnitudeVector2 = sqrt(magnitudePowerVector2);
        double dotProduct = vector.Vector2DotProduct(vector, vector2);
        double result = Math.acos(dotProduct / (abs(magnitudeVector1) * abs(magnitudeVector2)));
        return result;
    }

    public int getX() {
        return X;
    }

    public int getY() {
        return Y;
    }

    public void setX(int x) {
        X = x;
    }

    public void setY(int y) {
        Y = y;
    }
}
