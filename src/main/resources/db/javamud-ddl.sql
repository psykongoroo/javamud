CREATE TABLE "main"."grid_entity" (
  id      INTEGER PRIMARY KEY,
  width   INTEGER NOT NULL,
  height  INTEGER NOT NULL
);

CREATE TABLE "main"."grid_item_entity" (
  id      INTEGER PRIMARY KEY,
  grid_id INTEGER NOT NULL,
  x       INTEGER,
  y       INTEGER
);

CREATE TABLE "main"."grid_view_entity" (
  id      INTEGER PRIMARY KEY,
  grid_id INTEGER NOT NULL,
  minx    INTEGER,
  maxx    INTEGER,
  miny    INTEGER,
  maxy    INTEGER
);

CREATE VIRTUAL TABLE "main"."grid_item_rtree" USING rtree_i32(
  id,
  minX, maxX,
  minY, maxY,
);
