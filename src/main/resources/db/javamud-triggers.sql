CREATE TRIGGER "main"."trigger_grid_item_insert"
 AFTER INSERT ON "main"."grid_item_entity"
BEGIN INSERT INTO "grid_item_rtree" (id, minX, maxX, minY, maxY)
  VALUES (NEW.id, NEW.x, NEW.x, NEW.y, NEW.y);
END
;;

CREATE TRIGGER "main"."trigger_grid_item_delete"
 AFTER DELETE ON "main"."grid_item_entity"
BEGIN DELETE FROM "grid_item_rtree" WHERE "grid_item_rtree".id = OLD.id;
END
;;

CREATE TRIGGER "main"."trigger_grid_item_update"
 AFTER UPDATE ON "main"."grid_item_entity"
BEGIN UPDATE "grid_item_rtree"
  SET
    minX = NEW.x,
    maxX = NEW.x,
    minY = NEW.y,
    maxY = NEW.y
  WHERE "grid_item_rtree".id = NEW.id;
END
;;
