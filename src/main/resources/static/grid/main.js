var stompClient = null;

function connect(gridView) {
    var id = gridView.id;
    var gridId = gridView.gridId;
    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ', frame);
        stompClient.subscribe('/topic/grid/' + gridId + '/view/' + id, onGridUpdateMessage);
    });
}

function onGridUpdate(grid) {
    grid.gridItems.forEach(function (i) {
        $("#main-grid")
            .find("tr:nth-child(" + String(i.y + 1) + ")")
            .find("td:nth-child(" + String(i.x + 1) + ")")
            .text("♜");
    });
    return grid;
}

function onGridUpdateMessage(message) {
    console.log("Recevied", message);
    $("#main-grid td").text("");
    var grid = JSON.parse(message.body);
    onGridUpdate(grid);
}

function send() {
    var id = $("#id").val();
    var x = $("#x").val();
    var y = $("#y").val();
    stompClient.send('/app/grid/update/event', {}, JSON.stringify({
        gridId: 0,
        gridItems: [{id: id, x: x, y: y}]}));
}

function disconnect() {
    if (stompClient === null) {
        return;
    }
    stompClient.disconnect();
    console.log("Disconnected");
}

function resize_table_chess( id ) {
    $(id).width( 'auto' ).height( 'auto' );
    $(id+' td, '+id+' th').width( 'auto' ).height( 'auto' ).css({ 'font-size':0.1+'em' });
    // on redimensionne pour que le plateau tienne dans la fenêtre
    var sizT = Math.max( Math.max( $(id).width(), $(id).height()), Math.min( $(window).width(), $(window).height())-5 ); // -5px : marge
    $(id).width( sizT ).height( sizT );
    // on redimensionne les cases ET les pièces du jeu
    var maxWH = sizT/10; // 10x10 cases
    $(id+' td, '+id+' th').width( maxWH ).height( maxWH );
    $(id+' td').css({ 'font-size':Math.floor(100*maxWH/16/1.4)/100+'em' });
    $(id+' th').css({ 'font-size':Math.floor(100*maxWH/16/2.5)/100+'em' });
}

$(function () {
    $(window).on( 'load resize', function(){
        resize_table_chess('.chess_table');
    });
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() {
        $.ajax("/api/grid/view", {
            data: JSON.stringify({
                gridId: 0,
                minX: 0,
                minY: 0,
                maxX: 7,
                maxY: 7
            }),
            contentType: "application/json",
            method: "POST",
            dataType: "json"
        })
            .then(onGridUpdate)
            .then(connect)
            .catch(function (error) {
                console.log("Error:", error)
            });
    });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { send(); });
});
